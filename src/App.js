
import React,{Component} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Login from './components/Login';
import Admin from './components/Admin';
import Logout from './components/Logout';
import './App.css';

class App extends Component{
  render(){
    return(
      <Router>
   <div>
<Route exact path="/" component = {Login} />
<Route  path="/admin" component = {Admin} />
<Route  path="/logout" component = {Logout} />

   </div>
   </Router>
    )
  }
}
export default App;